int leftSensor = A0;
int rightSensor = A1;

int potMotor = A5;

int leftMotor = 3;
int rightMotor = 5;

bool esInicio = true;

int delayEmpuje = 11;
int empujeInicial = 150;

void setup() {
  Serial.begin(9600);
  
  pinMode(leftSensor, INPUT);
  pinMode(rightSensor, INPUT);

  pinMode(potMotor, INPUT);

  pinMode(leftMotor, OUTPUT);
  pinMode(rightMotor, OUTPUT);
}

void loop() {
  if (esInicio) {
    delay(250);
    analogWrite(leftMotor, empujeInicial);
    analogWrite(rightMotor, empujeInicial);
    delay(50);

    esInicio = false;
  }

  int potValue = analogRead(potMotor);
  int velMotor = map(potValue, 0, 1024, 0, 254);

  int rightValue = analogRead(leftSensor);

  if (rightValue > 750) {
    analogWrite(leftMotor, 254);
    delay(delayEmpuje);
    analogWrite(leftMotor, velMotor - (velMotor * .255));
  } else {
    analogWrite(leftMotor, 0);
    delay(5);
  }

//  Serial.println(rightValue);

  int leftValue = analogRead(rightSensor);

  if (leftValue > 450) {
    analogWrite(rightMotor, 254);
    delay(delayEmpuje);
    analogWrite(rightMotor, velMotor);
  } else {
    analogWrite(rightMotor, 0);
  }

//  Serial.println(leftValue);

  delay(50);
}
